provider "aws" {
    region = "us-east-1"

}

resource "aws_vpc" "main" {
    cidr_block = "172.40.0.0/16"
    instance_tenancy = "default"

}




resource "aws_subnet" "tf-public_subnet_1" {
    
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "172.40.96.0/20"
    availability_zone = "us-east-1a"

}

resource "aws_subnet" "tf-public_subnet_2" {
    
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "172.40.112.0/20"
    availability_zone = "us-east-1b"


}


resource "aws_subnet" "tf-private_subnet_1" {
    
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "172.40.128.0/20"
    availability_zone = "us-east-1a"

}

resource "aws_subnet" "tf-private_subnet_2" {
    
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "172.40.144.0/20"
    availability_zone = "us-east-1b"


}




resource "aws_security_group" "tf-project-sg-1" {
  name        = "tf-project-sg-1"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.40.0.0/32"]
    }

}

resource "aws_key_pair" "deployer" {
  key_name   = "tf-key-pair"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGMi3+x5C6B5sGdrkLHkYQkWEWVX1uXVuAVXEsLiwHf9 i160273@nu.edu.pk"

}