###############################################################################	
#	Muhammad Noffel Khan													  #
#	i16-0273																  #
																			  #		
###############################################################################	


import docker
import psutil
import os


client = docker.from_env()

#client.images.pull('cloud-project:testing')

port = 6000		#	setting port number
count = 0		#	total number of containers
a = 0			#	counter for port numbers 

##############################################################################################################################################################	
	
#	Infinite loop

while(True):

	
	x = 0
	cpu = os.getloadavg()[0]					# CPU load
	load = int((cpu / 10) * 100)				# Percentage CPU load

	N = int(load / 10) 							#	Calculating N according to the given formula 	

	count 	= len(client.containers.list())		#	Number of active containers

	# Number of active containers is less than N
	
	if (count < N):
		print ('\nCPU Utilization: ' + str(load) + '%' + '\n')		
		print('\nContainer list: ')
		for container in client.containers.list():
			print (container.name)	
			
		count 	= len(client.containers.list())		#	Number of active containers
		print ('\nTotal containers: ' + str(count) + '\n\n')	

##############################################################################################################################################################	

	
	#	Add container(s) as CPU load INCREASES 
	
	if (N >= 1):
		
		count 	= len(client.containers.list())		#	Number of active containers
		proxyFile = open("/etc/haproxy/haproxy.cfg", "a")	#	Open HAProxy configuration file 

		for n in range(count, N):
			a = a + 1		
			proxyFile.write("    server node-{}  127.0.0.1:{} \n".format(a,port+a))
			container = client.containers.create('cloud-project:done',ports={'5000/tcp':port+a})
			container.start()
			print('\nCreating container number {}'.format(a))	
			container.rename("container-{}".format(a))
			print ('\nCreated container-{} added successfully!\n'.format(a))		
			print('\n\nContainer list: ')
			for container in client.containers.list():
				print (container.name)	
				
			count 	= len(client.containers.list())		#	Number of active containers
			print ('\nTotal containers: ' + str(count) + '\n\n')			
						
		
		proxyFile.close()

##############################################################################################################################################################	



    #	Remove container(s) when CPU load DECREASES
     						
	elif (N < len(client.containers.list())):
		print ('CPU Utilization: ' + str(load) + '%' + '\n')		
		print('N < number of containers running')

		for i in range(0, count-N):
			client.containers.list()[count-i].stop()	
			client.containers.list()[count-i].remove()
		
		print ('\nTotal containers: ' + str(count) + '\n\n')			
		


###############################################################################	





		
		#containerList = client.containers.list()
		#for container in containerList:
			#while (len(containerList) > 1):
				#container.stop()
				#container.remove()

		#container = client.containers.create('cloud-project:testing',ports={'5000/tcp':port+1})
		


	#print ('\n\n')

	#container_list = client.containers.list()

#add

	#print('Summary: Online Containers: {} Offline Containers: {}'.format(count,len(container_list)-count))
	#print('Removing containers...')

	#Let's clean up and put our toys back in the toybox.
	#for container in container_list:
	#  container.stop()
	#  container.remove()
