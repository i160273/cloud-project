from flask import Flask
import socket


app = Flask(__name__)


@app.route('/')
def index():
	return "-----" + "Hostname = " + socket.gethostname() + "-----" + "Name: Noffel" + "-----" + "Roll Number = i160273"


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
